
const initialState = {
  isAuthenticated: false, 
  token:null, 
  loginError: false,
  registerError: false,
  registerOk: false,
 };

const auth =  (state = initialState, action) => {
  switch (action.type) {
    
    case 'SET_TOKEN':

      return  {
        ...state,
        isAuthenticated: true,
        token: action.payload.authToken,
        loginError: false,
      }
      

    case 'SET_LOGIN_ERROR':
      return  {
        ...state,
        loginError: action.payload,
      }

    case 'SET_REGISTER_ERROR':
      return  {
        ...state,
        registerError: action.payload,
        registerOk: false
      }

      case 'SET_REGISTER_OK':
        return  {
          ...state,
          registerOk: action.payload,
          registerError: false
        }

      
    default:
      return state;
  }
}

export default auth;