import axios from 'axios';

const API_URL = "http://localhost:5000/";

export const authService = {
    async login(loginData) {
        const res = await axios.post(API_URL + 'login', loginData);
        return {data: res.data};
    },
    async register(registerData) {
        const res = await axios.post(API_URL + 'register', registerData);
        return {data: res.data};
    },
}  



