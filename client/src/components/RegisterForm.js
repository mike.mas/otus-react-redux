import React, { useState } from "react";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useSelector, useDispatch } from 'react-redux';
import {register} from  '../actions/auth';

function RegisterForm() {
   
  const dispatch = useDispatch();
  const registerError = useSelector(({ auth }) => auth.registerError);
  const registerOk = useSelector(({ auth }) => auth.registerOk);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  

  const handleSubmit =  (e) => {
    e.preventDefault();
    
    if (!username || !password)
      return; 
    
    dispatch(register({username, password})); 
}
  
  return (
      <form onSubmit={handleSubmit} className="form">
      <div>{registerError ? "Registration error" : ""}
           {registerOk ? "Registration succeeded" : ""}
      </div>

      <div> 
      <TextField id="username"  value={username} onChange={e => setUsername(e.target.value)}  label="Username" fullWidth   />
      </div>

      <div>
      <TextField id="password"   value={password} onChange={e => setPassword(e.target.value)}  label="Password" type="password" fullWidth />
      </div>

      <div>
        <Button type="submit" variant="contained">Register</Button>

      </div>

      </form>
    );
  }
  
  export default RegisterForm;
  