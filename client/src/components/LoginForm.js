import React, { useState } from "react";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useSelector, useDispatch } from 'react-redux';
import {login} from  '../actions/auth';

function LoginForm() {
   
  const dispatch = useDispatch();
  const loginError = useSelector(({ auth }) => auth.loginError);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  

  const handleSubmit =  (e) => {
    e.preventDefault();
    
    if (!username || !password)
      return;
    
    dispatch(login({username, password})); 
}
  
  return (
      <form onSubmit={handleSubmit} className="form">
      <div>{loginError ? "Wrong credentials" : ""}</div>

      <div> 
      <TextField id="username"  value={username} onChange={e => setUsername(e.target.value)}  label="Username" fullWidth   />
      </div>

      <div>
      <TextField id="password"   value={password} onChange={e => setPassword(e.target.value)}  label="Password" type="password" fullWidth />
      </div>

      <div>
        <Button type="submit" variant="contained">Login</Button>

      </div>

      </form>
    );
  }
  
  export default LoginForm;
  