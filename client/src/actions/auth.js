import {authService} from '../services/authService';

 export const login = (userData) => async (dispatch) => {
    
    try {
        const loginResult = await authService.login(userData);
      
        dispatch({
            type: 'SET_TOKEN',
            payload: loginResult.data,
        });
    } catch {
        dispatch({
            type: 'SET_LOGIN_ERROR',
            payload: true,
        });
    }
}

    export const register = (userData) => async (dispatch) => {
    
        try {
            const registerResult = await authService.register(userData);
            dispatch({
                type: 'SET_REGISTER_OK',
                payload: true,
            });
        } catch {
            dispatch({
                type: 'SET_REGISTER_ERROR',
                payload: true,
            });
        }

    
};
