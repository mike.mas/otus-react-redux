import {BrowserRouter as Router, Switch, Route, Link  } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import Home from "./components/Home";
import LoginForm from "./components/LoginForm";
import RegisterForm from "./components/RegisterForm";
import NotFound from "./components/NotFound";

function App() {
  return (
    
    <Provider store={store}>
    <div className="App">
      <Router>

      <header> 
        <Link to="/">home</Link> | <Link to="/login">login</Link> | <Link to="/register">register</Link> | <Link to="/abyrvalg">not found</Link>
      </header>

        <Switch>
              <Route path="/" component={Home} exact />
              <Route path="/login" component={LoginForm} exact />
              <Route path="/register" component={RegisterForm} exact />
              <Route  component={NotFound}  />
        </Switch>

      </Router>
    </div>
    </Provider>
  );
}

export default App;
