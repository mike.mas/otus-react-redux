const express = require('express');
const cors = require('cors')
const app = express();
const port = 5000;
app.use(cors());
app.use(express.json());

let users = [];

app.post('/login', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;   
   
    if (users.find(el => el.username == username && el.password == password ))
        res.send({status:"ok", authToken: `test token for ${username}`});
    else 
        res.status(401).send("Wrong credentials");
    
});

app.post('/register', (req, res) => {
   
    const username = req.body.username;
    const password = req.body.password;

    if (username && password && !users.find(el => el.username == username))
       {
            users.push({username:username, password: password})
            res.send({status:"ok"});
        }
    else 
        res.status(400).send("Wrong data");
    
});

app.listen(port, () => console.log(`authApi app listening on port ${port}!`))
